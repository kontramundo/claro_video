import React from 'react';

export const NotFound = () => (
    <div className="content-fluid">
        <div className="row">
            <div className="col-12 text-white text-center m-5">
                <i class="fas fa-exclamation-triangle fa-5x"></i>
                <h1 className="title">404</h1>
                <h2 className="subtitle">PÁGINA NO ENCONTRADA</h2>
            </div>
        </div>
    </div>
)