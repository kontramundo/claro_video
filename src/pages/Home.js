import React,{Component} from 'react';
import Header from '../components/Header';
import NavBar from '../components/NavBar';
import ContentList from '../components/ContentList';

export default class Home extends Component {

    render() {
        return (
            <React.Fragment>
                <Header/>
                <NavBar/>
                <ContentList
                    cat = {this.props.match.params.cat}
                />
            </React.Fragment>
        );
    }
}