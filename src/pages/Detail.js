import React,{Component} from 'react';
import Header from '../components/Header';
import NavBar from '../components/NavBar';
import ContentDetail from '../components/ContentDetail';

export default class Detail extends Component {

    render() {
        return (
            <React.Fragment>
                <Header/>
                <NavBar/>
                <ContentDetail
                    id = {this.props.match.params.id}
                />
            </React.Fragment>
        );
    }
}