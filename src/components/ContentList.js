import React,{Component} from 'react';
import Content from './Content';

//Redux
import { connect } from 'react-redux';
import { getCategory } from '../actions/listActions';


class ContentList extends Component {

    componentDidMount(){
        this.props.getCategory('gen_accion');
    }

    componentDidUpdate(prevProps) {

        if (this.props.cat !== prevProps.cat) {
            this.props.getCategory(this.props.cat);
        }
    }

    render() {
        const { contents, search, contentsSearch } = this.props

        let contentsData = search===true ? contentsSearch : contents;

        return (

            <div className="container-fluid">
                <div className="row no-gutters">
                    {
                        contentsData.map((content, key) => (
                            <Content 
                                key={key}
                                id={content.id}
                                title={content.title}
                                image={content.image_small}
                            />
                        ))
                    }

                    {
                        search===true && contentsData.length<=0 
                        ? 
                            <div className="col-12 text-center text-white m-5">
                                <h1><i class="fas fa-exclamation-triangle"></i><br/> ¡Lo sentimos! Parece que no hay coincidencias</h1> 
                                <p>Utiliza otras palabras clave</p>
                                <p>Prueba con otro género</p>
                            </div>
                        : 
                            ''
                    } 
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    search: state.List.search,
    contents: state.List.contents,
    contentsSearch: state.List.contentsSearch,
})

export default connect(mapStateToProps, { getCategory })(ContentList);