import React,{Component} from 'react';

export default class Content extends Component {

    render() {
        const { id, title, image } = this.props

        const contentImage = image ? image : 'https://via.placeholder.com/286x180.png?text=Puppy+Recipe';

        return (
            <div className="col-xl-2 col-lg-3 col-md-4 col-sm-6 sol-xs-12">
               <div className="card m-2 shadow-sm">
                    <a href={`detail/${id}`}><img src={contentImage} className="card-img-top" alt={title} /></a>
                </div>
            </div>
        );
    }
}