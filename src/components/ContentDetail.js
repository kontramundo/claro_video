import React,{Component} from 'react';

//Redux
import { connect } from 'react-redux';
import { getDetail } from '../actions/detailActions';

class ContentDetail extends Component {

    componentDidMount() {
        const id = this.props.id

        this.props.getDetail(id);
        
    }

    render() {

        if(!this.props.content)
        {
            return  (
                <div className="col-12 text-center text-white m-5">
                    <h1><i class="fas fa-exclamation-triangle"></i><br/> ¡Lo sentimos! Parece que no hay coincidencias</h1> 
                    <p>Utiliza otras palabras clave</p>
                    <p>Prueba con otro género</p>
                </div>
            );
        }

        const { image_large, title, large_description, duration, extendedcommon } = this.props.content

       if (!extendedcommon) {
        return null;
      }

        const originaltitle = extendedcommon.media.originaltitle
        const publishyear = extendedcommon.media.publishyear
        const genre = extendedcommon.genres.genre[0].desc
        const role = extendedcommon.roles.role

        return (
            <div className="container-fluid">
                
                <div className="row text-white">
                    <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <img src={image_large} className="img-fluid" alt={title} />
                    </div>
                    <div className="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12">
                        <h5 className="text-warning"><i class="fas fa-film"></i> {title}</h5>
                        <p><small className="text-muted">{ `${originaltitle} (${publishyear})`}</small></p>
                        <p>{large_description}</p>
                        <p><span className="text-muted"><strong><i class="fas fa-clock"></i> Duración:</strong> {duration}</span></p>
                        <p><span className="text-muted"><strong><i class="fas fa-project-diagram"></i> Género:</strong> {genre}</span></p>
                        {
                            role.map((actor, key) =>  {
                                return (
                                    <React.Fragment key={key}>
                                        <p>{actor.desc}: 
                                            <br/>
                                            {
                                                actor.talents.talent.map((talent, key2) =>  {
                                                    return (
                                                        <span key={key2} className="badge badge-secondary mr-1">{talent.fullname}</span>
                                                    );     
                                                })
                                            }
                                        </p>
                                    </React.Fragment>
                                );
                            })
                            
                        }
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    content: state.Detail.content,
})

export default connect(mapStateToProps, { getDetail })(ContentDetail);