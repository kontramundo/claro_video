import React,{Component} from 'react';
import { Link } from 'react-router-dom'

export default class NavBar extends Component {

    constructor(props) {
        super(props);

        this.state = {
            activeIndex: 0
        }

        this.handleClick = this.handleClick.bind(this);
    }
    handleClick(event, index) {
          

        this.setState({ activeIndex: index });
    }



    render() {

        const { activeIndex } = this.state

        return (
            <div className="nav-scroller py-1 mb-2 bg-dark">
                <nav className="nav d-flex justify-content-between">
                    <Link to='/gen_accion' className={activeIndex===0 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 0)}>Acción y Aventura</Link>
                    <Link to='/gen_anime' className={activeIndex===1 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 1)}>Animé y Videojuegos</Link>
                    <Link to='/gen_biograficas' className={activeIndex===2 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 2)}>Biográficas</Link>
                    <Link to='/gen_scifi' className={activeIndex===3 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 3)}>Ciencia Ficción</Link>
                    <Link to='/gen_cineoro' className={activeIndex===4 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 4)}>Cine de Oro</Link>
                    <Link to='/gen_clasicas' className={activeIndex===5 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 5)}>Clásicas</Link>
                    <Link to='/gen_comedia' className={activeIndex===6 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 6)}>Comedia</Link>
                    <Link to='/gen_deportes' className={activeIndex===7 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 7)}>Deportes</Link>
                    <Link to='/gen_docu' className={activeIndex===8 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 8)}>Documentales</Link>
                    <Link to='/gen_drama' className={activeIndex===9 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 9)}>Drama</Link>
                    <Link to='/gen_familiares' className={activeIndex===10 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 10)}>Familiares</Link>
                    <Link to='/gen_historicas' className={activeIndex===11 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 11)}>Históricas</Link>
                    <Link to='/gen_infantiles' className={activeIndex===12 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 12)}>Infantiles</Link>
                    <Link to='/gen_latino' className={activeIndex===13 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 13)}>Latinoamericanas</Link>
                    <Link to='/gen_musica' className={activeIndex===14 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 14)}>Música</Link>
                    <Link to='/gen_romanticas' className={activeIndex===15 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 15)}>Románticas</Link>
                    <Link to='/gen_terror' className={activeIndex===16 ? 'text-white mr-2': 'text-muted mr-2'} onClick={(e) => this.handleClick(e, 16)}>Terror y Suspenso</Link>
                </nav>
            </div>
        );
    }
}