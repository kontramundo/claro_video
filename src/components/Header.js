import React,{Component} from 'react';
import logo from '../images/clarovideo-logo-sitio.svg';

//Redux
import { connect } from 'react-redux';
import { search } from '../actions/listActions';


class Header extends Component {

    constructor(props) {
        super(props);

        this.state = {value: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const { value } = this.state

        if(value.length>0)
        {
            this.props.search(value)
        }
    }

    render() {
        return (
            <header>
                <div className="navbar navbar-dark bg-dark shadow-sm">
                    <div className="container-fluid d-flex justify-content-between">
                        <a href="/" className="navbar-brand d-flex align-items-center" >
                            <img src={logo} alt="Claro Video" width={120}/>
                        </a>
                        <form onSubmit={this.handleSubmit}>
                            <div className="input-group">
                                <input 
                                    type="text" 
                                    name="search"
                                    className="form-control" 
                                    placeholder="Buscar" 
                                    required
                                    value={this.state.value} 
                                    onChange={this.handleChange}
                                />
                                <div className="input-group-append">
                                    <button type="submit" className="btn btn-secondary" ><i className="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </header>
        )
    }
}


export default connect(null, { search })(Header);
