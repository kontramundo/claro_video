import React,{Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

//Redux
import { Provider } from 'react-redux'
import store from './store'

//Components
import Home from './pages/Home';
import Detail from './pages/Detail';
import { NotFound } from './pages/NotFound'

export default class App extends Component {

    render() {
        return (
            <Provider store={store}>
                <Router>
                    <Switch>
                        <Route exact path="/:cat?" component={Home}/>
                        <Route exact path="/detail/:id" component={Detail}/>
                        <Route component={NotFound} />
                    </Switch>
                </Router>
            </Provider>
        );
    }
}