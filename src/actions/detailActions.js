import { GET_DETAIL } from './types';
import axios from 'axios'

export const getDetail = (id) => async dispatch => {

    let detail = '';

    detail = await axios.get(`https://mfwkweb-api.clarovideo.net/services/content/data?device_id=web&device_category=web&device_model=web&device_type=%20web&format=json&device_manufacturer=generic&authpn=webclient&authpt=tfg1h3j4k6fd7&api_version=v5.86&region=mexico&HKS%20=9s5hqq76r3g6sg4jb90l38us52&user_id=22822863&group_id=${id}`)
    .then(function(response){

        return response.data.response.group.common
    })
    .catch(function (error) {
        // handle error
    });

    dispatch({
        type: GET_DETAIL,
        payload: detail
    });
}