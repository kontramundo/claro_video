import { GET_CATEGORY, SEARCH } from './types';
import axios from 'axios'

export const getCategory = (category) => async dispatch => {

    let url, contents = '';

    url = await axios.get(`https://mfwkweb-api.clarovideo.net/services/cms/level?api_version=v5.86&authpn=webclient&authpt=tfg1h3j4k6fd7&format=json&region=mexico&device_id=web&device_category=web&device_model=web&device_type=web&device_manufacturer=generic&HKS=9s5hqq76r3g6sg4jb90l38us52&node=${category}`)
    .then(function(response){

        return response.data.response.modules.module[0].components.component[2].properties.url  
    })
    .catch(function (error) {
        // handle error
    });


    contents = await  axios.get(`https://mfwkweb-api.clarovideo.net${url}&&api_version=v5.86&authpn=webclient&authpt=tfg1h3j4k6fd7&format=json&region=mexico&device_id=web&device_category=web&device_model=web&device_type=web&device_manufacturer=generic&HKS=9s5hqq76r3g6sg4jb90l38us52`)
    .then(function(response){

        return response.data.response.groups    
    })
    .catch(function (error) {
        // handle error
    });

    dispatch({
        type: GET_CATEGORY,
        payload: contents
    });
}

export const search = (search) => {
    
    return {
        type: SEARCH,
        payload: search
    } 
}