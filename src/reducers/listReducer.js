import { GET_CATEGORY, SEARCH } from '../actions/types'

const initialState = {
    search: false,
    contents:[],
    contentsSearch: []
}

export default function(state = initialState, action){
    switch(action.type){
        case GET_CATEGORY:
            return {
                ...state,
                contents: action.payload,
                search: false
            }
        case SEARCH:
            return {
                ...state,
                search:true,
                contentsSearch: state.contents.filter(content => content.title.toLowerCase().includes(action.payload.toLowerCase()))
            }
        default:
            return state
    }
}