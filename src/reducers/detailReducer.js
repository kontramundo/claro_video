import { GET_DETAIL } from '../actions/types'

const initialState = {
    content: []
}

export default function(state = initialState, action){
    switch(action.type){
        case GET_DETAIL:
            return {
                ...state,
                content: action.payload
            }
        default:
            return state
    }
}